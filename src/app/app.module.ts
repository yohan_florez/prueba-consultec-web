import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//components
import { HeaderComponent } from './components/header/header.component';
import { ClientViewComponent } from './components/client-view/client-view.component';

//views
import { PagePrincipalComponent } from './pages/page-principal/page-principal.component';

//Plugins
import { BlockUIModule } from 'ng-block-ui';
import { CustomFormsModule } from 'ng2-validation'

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PagePrincipalComponent,
    ClientViewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BlockUIModule.forRoot(),
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    CustomFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
