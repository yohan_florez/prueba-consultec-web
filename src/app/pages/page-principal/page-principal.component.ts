import { Component, OnInit } from '@angular/core';
import { ServiceModule } from 'src/app/services/service.module';
import { UtilService } from 'src/app/services/util.service';
import { Client } from 'src/app/models/client';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-page-principal',
  templateUrl: './page-principal.component.html',
  styleUrls: ['./page-principal.component.scss']
})
export class PagePrincipalComponent implements OnInit {
 
  client: Client;
  customerEdit:any;
  clientView : any;
  search = '+';
  clients:any = [];
  citys:any = [];
  states:any = [];
  view:boolean = false;
  add:boolean = false;
  edit:boolean = false;
  submitted:boolean = false;
  guarded:boolean;
  edited:boolean;

  constructor(private service: ServiceModule, private utilService: UtilService) {
    this.client = new Client();
   }

  ngOnInit() {
    this.getUsers();
    this.getCitys();
    this.getStatus();
  }

  editClient(c){
    this.client = c;
    this.edit = true;
    this.add = !this.add;
  }

  viewClient(c){
    this.clientView = c;
    this.proveViewClient();
  }

  prove(){
    this.add = !this.add;
    this.edit = false;
    this.client = new Client();
  }

  proveViewClient(){
    this.view = !this.view;
  }

  getUsers(){
    this.utilService.blockUiStart();
    this.service.getAll('client').subscribe(data => {
      this.utilService.blockUiStop();
      this.clients = data;
    });
  }

  getCitys(){
    this.utilService.blockUiStart();
    this.service.getAll('city').subscribe(data =>{
      this.utilService.blockUiStop();
      this.citys = data;
    })
  }

  getStatus(){
    this.utilService.blockUiStart();
    this.service.getAll('get-state').subscribe(data =>{
      this.utilService.blockUiStop();
      this.states = data;
    });
  }

  addCient(form){
    this.submitted = true;
    if(form && !form.valid)
      return;
      if(!this.edit){
        this.utilService.blockUiStart();
        this.service.saveOrUpdate('client', this.client).subscribe(data =>{
          this.utilService.blockUiStop();
          this.guarded = true;
          setTimeout(() => { this.guarded = false; }, 3000);
          this.getUsers();
          this.prove();
          this.client = new Client();
          this.submitted = false;
          this.edit = true;
        }); 
      }
      if(this.edit){
        this.utilService.blockUiStart();
        this.service.saveOrUpdate('update-client', this.client, false).subscribe(data =>{
          this.utilService.blockUiStop();
          this.edited = true;
          setTimeout(() => { this.edited = false; }, 3000);
          this.getUsers();
          this.prove();
          this.client = new Client();
          this.submitted = false;
        });
      } 
  }

  inactiveClient(c){
    this.client = c
    Swal.fire({
      title: 'Seguro que quiere eliminar el cliente?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'Cancelar'
    }).then((result) => {  
      if (result.value) {
        this.utilService.blockUiStart();
        this.service.postRequest('delete-inactive', {status: 2, id_client: this.client.id }).subscribe(data =>{
          this.utilService.blockUiStop();
          Swal.fire(
            'Eliminado!',
            'El cliente ha sido eliminado',
            'success'
          )
          this.getUsers();
        });  
      }
    });
    
  }
}
