import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagePrincipalComponent } from './pages/page-principal/page-principal.component';

const routes: Routes = [
  {pathMatch: 'full', path: '', component: PagePrincipalComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
