export interface Products {
    id:any
    pr_img:string;
    pr_name:string;
    pr_description:string;
    pr_price:number;
    pr_quantity:number;
    priceItemsTotal:number;
    totalPayment:number;
    cantSelect:number;
}
