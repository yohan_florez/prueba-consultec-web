import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-client-view',
  templateUrl: './client-view.component.html',
  styleUrls: ['./client-view.component.scss']
})
export class ClientViewComponent implements OnInit {

  @Input() clientView:any;

  constructor() {
    
   }

  ngOnInit() {
    console.log(this.clientView)
  }

}
